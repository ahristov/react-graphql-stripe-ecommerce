# Build an Online Store with React and GraphQL in 90min

This repository contains code and notes from studying the Udemy course [Build an Online Store with React and GraphQL in 90min](https://www.udemy.com/build-an-online-store-with-react-and-graphql-in-90-minutes)

Links:

[React spinners](http://www.davidhu.io/react-spinners/)

[React gestalt](https://pinterest.github.io/gestalt/#/)

## Section 2: Setup

This section is about setting up te app, routing and navigation.

Install required packages:

```bash
cd client
npm i --save react react-dom react-router-dom react-scripts react-spinners react-stripe-elements gestalt strapi-sdk-javascript
npm start
```

### Lesson 02.03: Initial setup

Branch: 02.03-InitialSetup

### Lesson 02.04-05: Create Components and Routing, Hot module load

Branch: 02.04-ComponentsAndRouting

### Lesson 02.06-07: Navbar and NavLink style

Branch: 02.06-Navbar

## Section 3: Add Strapi and GraphQL

### Lesson 03.08: MLab Database, Install Strapi CLI, Create Strapi Project

Install the latest version of the `strapi-cli` globally:

```bash
npm i -g strapi@alpha
```

Then we create new app at the same level as the _client_ folder.

```bash
strapi new server
```

We choose MongoDB as a server. Then we supply the connection settings copying from the dashboard of the database we created at the MLab site:

```bash
mongo ds027769.mlab.com:27769/brewhaha -u <dbuser> -p <dbpassword>
```

Or from he above example:

- Database schema: brewhaha
- Host name: ds027769.mlab.com
- Port: 27769
- Authentication database: brewhaha
- Enable SSL: false

It will create the server solution and the above configuration will set added into the file:

```bash
server/config/environments/development/database.json:
```

Strapi provides a CMS to interact with the data from the database.

Strapi gives an access to different commands. Run:

```bash
strapi help
```

to take a look of some of these.

Use `strapi install` to install a plugin.

Use `strapi generate:api` to generate API without UI, to really quick bootstrap.

The complete documentation is at [Strapi CLI documentation](https://strapi.io/documentation/cli/CLI.html).

### Lesson 03.10: Create root admin

We start the server:

```bash
cd server
strapi start
```

### Lesson 03.11: Admin panel

And open in the browser (I changed the IP):

    http://192.168.0.16:1337/admin

and create admin user:

![Admin](./assets/img/admin_user_created1.png)

If we look at the MLab database we have bunch of collections corresponding to the roles and permissions:

![DB tables](./assets/img/db_access_permissions1.png)

### Lesson 03.12: Add Brand content type

We add new content type "Brand", to the "default" collection:

![Brand content type](./assets/img/brand_content_type_1.png)

After content type added, the Strapi server restarts. After that it will be presented on the left side:

![Brand menu](./assets/img/brand_content_type_menu_1.png)

We add brand using the data from the _brands.json_ file and image from the _./brands-images_ directory:

![First brand](./assets/img/first_brand1.png)

### Lesson 03.12: Add GraphQL plugin

Add GraphQL plugin. After the server restarted we can open the GraphQL console at:

    http://192.168.0.16:1337/graqhql

**Important**: There is an issue with this version of GraphQL. After installed the plugin, go to:

```bash
cd server/plugins/graphql
```

and edit file `package.json`.

Change this:

```javascript
"graphql": "^14.0.2",
```

to this:

```javascript
"graphql": "^0.13.2",
```

and run:

```bash
npm install
```

See: [GraphQL Fix for _id](https://www.udemy.com/build-an-online-store-with-react-and-graphql-in-90-minutes/learn/v4/questions/5302334)

## Section 4: GraphQL queries

Let's fetch the only brand, as we can see on the right side, the schema is already provided to us:

![First GraphQL query](./assets/img/first_graphql_query1.png)

We need to allow the "Public" role can fetch individual brand (`findnode`) and we also allow only authenticated users to be able to fetch limited number of times:

![Brand role permissions](./assets/img/brand_role_permissions1.png)

We repeat that for both "Authenticated" and "Public" roles:

![Authenticated and public roles access to findnode on Brand](./assets/img/brand_role_permission2.png)

Repeat the same for `find`, so both "Authenticated" and "Public" roles can fetch list of nodes.

## Section 5: Query brands in with GraphQL in react App, Display Brands in UI

## Section 6: Display Brands in UI: Additional features, responsive design
