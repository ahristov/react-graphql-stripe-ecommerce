import React, { Component } from 'react';
import { Container, Box, Button, Heading, Text, TextField } from 'gestalt';
import { css } from 'emotion';
import ToastMessage from './ToastMessage';
import { setToken } from '../utils';

import Strapi from 'strapi-sdk-javascript/build/main';
const apiUrl = process.env.API_URL || 'http://192.168.0.16:1337';
const strapi = new Strapi(apiUrl);

class Signup extends Component {
  state = {
    username: '',
    email: '',
    password: '',
    toastShow: false,
    toastMessage: '',
    loading: false
  };

  handleChange = ({ event, value }) => {
    event.persist();
    this.setState({ [event.target.name]: value });
  };

  handleSubmit = async event => {
    event.preventDefault();

    const { username, email, password } = this.state;

    if (this.isFormEmpty(this.state))
    {
      this.showToast('Fill in all fields');
      return;
    }

    // Sign up the user
    try
    {
      this.setState({ loading: true});
      const response = await strapi.register(username, email, password);
      this.setState({ loading: false});
      setToken(response.jwt);
      console.log(response);
      this.redirectUser('/');
    }
    catch(err) {
      this.setState({ loading: false});
      this.showToast(err.message);
    }
  }

  redirectUser = path => this.props.history.push(path);

  isFormEmpty = ({ username, email, password }) => {
    return !username || !email || !password;
  }

  showToast = toastMessage => {
    this.setState({ toastShow: true, toastMessage });
    setInterval(() => this.setState({ toastShow: false, toastMessage: '' }), 5000);
  }

  render() {
    const { username, email, password, toastShow, toastMessage, loading } = this.state;

    return (
      <Container>
        <Box
          dangerouslySetInlineStyle={{
            __style: {
              backgroundColor: '#ebe2da'
            }
          }}
          margin={4}
          padding={4}
          shape="rounded"
          display="flex"
          justifyContent="center"
        >
          {/* Sign Up Form */}
          <form 
            className={css`
              display: inline-block;
              text-align: center;
              maxWidth: 450px;
            `}
            onSubmit={this.handleSubmit}
          >
            {/* Sign Up Form Heading */}
            <Box
              marginBottom={2}
              display="flex"
              direction="column"
              alignItems="center"
            >
              <Heading color="midnight">Let's Get Started</Heading>
              <Text italic color="orchid">Sign up to order some brews!</Text>
            </Box>
            {/* Username Input */}
            <TextField
              id="username"
              name="username"
              type="text"
              placeholder="Username"
              onChange={this.handleChange}
              value={username}
            />
            {/* Email Input */}
            <TextField
              id="email"
              name="email"
              type="email"
              placeholder="Email Address"
              onChange={this.handleChange}
              value={email}
            />
            {/* Password Input */}
            <TextField
              id="password"
              name="password"
              type="password"
              placeholder="Password"
              onChange={this.handleChange}
              value={password}
            />
            <Button
              inline
              disabled={loading}
              color="blue"
              type="submit"
              text="Submit"
            />
          </form>
        </Box>
        <ToastMessage
          show={toastShow}
          message={toastMessage}
        />
      </Container>
    );
  }
}


export default Signup;
