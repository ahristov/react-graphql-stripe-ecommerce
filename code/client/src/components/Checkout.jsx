import React from 'react';
import { Container, Box, Button, Heading, Text, TextField, Modal, Spinner } from 'gestalt';
import { css } from 'emotion';
import { withRouter } from 'react-router-dom';
import ToastMessage from './ToastMessage';
import { getCart, calculatePrice, clearCart, calculateAmount } from '../utils';
import { Elements, StripeProvider, CardElement, injectStripe } from 'react-stripe-elements';

import Strapi from 'strapi-sdk-javascript/build/main';
const apiUrl = process.env.API_URL || 'http://192.168.0.16:1337';
const strapi = new Strapi(apiUrl);


const ConfirmationModal = ({ orderProcessing, cartItems, closeModal, handleSubmitOrder }) => (
  <Modal
    accessibilityCloseLabel="close"
    accessibilityModalLabel="Confirm Your Order"
    heading="Confirm Your Order"
    onDismiss={closeModal}
    footer={
      <Box display="flex" marginRight={-1} marginLeft={-1} justifyContent="center">
        <Box padding={1}>
          <Button
            size="lg"
            color="red"
            text="Submit"
            disabled={orderProcessing}
            onClick={handleSubmitOrder}
          />
        </Box>
        <Box padding={1}>
          <Button
            size="lg"
            text="Cancel"
            disabled={orderProcessing}
            onClick={closeModal}
          />
        </Box>        
      </Box>
    }
    role="alertdialog"
    size="sm"
  >
    {/* Order Summary */}
    {!orderProcessing && (
      <Box display="flex" justifyContent="center" alignItems="center" direction="column" padding={2} color="lightWash">
        {cartItems.map(item => (
          <Box key={item._id} padding={1}>
            <Text size="lg" color="red">
              {item.name} x {item.quantity} - ${item.quantity * item.price}
            </Text>
          </Box>
        ))}
        <Box paddingY={2}>
          <Text size="lg" bold>
            Total: {calculatePrice(cartItems)}
          </Text>
        </Box>
      </Box>
    )}
    {/* Order Processing Spinner */}
    <Spinner show={orderProcessing} accessibilityLabel="Order Processing Spinner" />
    {orderProcessing && <Box paddingY={3}><Text align="center" italic>Submitting Order...</Text></Box>}
  </Modal>
);

class _CheckoutForm extends React.Component {
  state = {
    cartItems: [],
    address: '',
    postalCode: '',
    city: '',
    confirmationEmailAddress: '',
    toastShow: false,
    toastMessage: '',
    orderProcessing: false,
    modal: false,
  }

  componentDidMount() {
    this.setState({ cartItems: getCart() });
  }

  handleChange = ({ event, value }) => {
    event.persist();
    this.setState({ [event.target.name]: value });
  };

  handleSubmitOrder = async () => {
    const { cartItems, city, address, postalCode, confirmationEmailAddress } = this.state;
    const amount = calculateAmount(cartItems);

    // Process order
    this.setState({ orderProcessing: true });
    try {
      const response = await this.props.stripe.createToken();
      const token = response.token.id;

      await strapi.createEntry('orders', {
        amount,
        brews: cartItems,
        city,
        postalCode,
        address,
        token
      });

      await strapi.request('POST', '/email', {
        data: {
          to: confirmationEmailAddress,
          subject: `Order Confirmation - BrewHaha ${new Date(Date.now())}`,
          text: 'Your order has been processed',
          html: '<bold>Expect your order to arrive soon!</bold>'
        }
      });
      
      this.setState({ orderProcessing: false, modal: false });
      clearCart();
      this.showToast('Your order has been successfully submitted!', true);

    } catch(err) {
      this.setState({ orderProcessing: false, modal: false });
      this.showToast(err.message);
    }
  };

  handleConfirmOrder = async event => {
    event.preventDefault();

    if (this.isFormEmpty(this.state))
    {
      this.showToast('Fill in all fields');
      return;
    }

    this.setState({ modal: true });
  };

  isFormEmpty = ({ address, postalCode, city, confirmationEmailAddress }) => {
    return !address || !postalCode || !city || !confirmationEmailAddress;
  };

  showToast = (toastMessage, redirect = false) => {
    this.setState({ toastShow: true, toastMessage });
    setInterval(
      () => this.setState({ toastShow: false, toastMessage: '' },
      () => redirect && this.redirectUser('/')
    ), 5000);
  };
  
  closeModal = () => this.setState({ modal: false });

  redirectUser = path => this.props.history.push(path);

  render() {
    const { cartItems, address, postalCode, city, confirmationEmailAddress, toastShow, toastMessage, orderProcessing, modal } = this.state;

    return (
      <Container>
        <Box
          color="darkWash"
          margin={4}
          padding={4}
          shape="rounded"
          display="flex"
          justifyContent="center"
          alignItems="center"
          direction="column"
        >
          {/* Checkout Form Heading */}
          <Heading color="midnight">Checkout</Heading>
          {cartItems.length > 0 ? <React.Fragment>
            {/* User Cart */}
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              direction="column"
              marginTop={2}
              marginBottom={6}
            >
              <Text color="darkGray" italic>{cartItems.length} Items for Checkout</Text>
              <Box padding={2}>
                {cartItems.map(item => (
                  <Box key={item._id} padding={1}>
                    <Text color="midnight">
                      {item.name} x {item.quantity} - ${item.quantity * item.price}
                    </Text>
                  </Box>
                ))}
              </Box>
              <Text bold>Total Amount: {calculatePrice(cartItems)}</Text>
            </Box>
            {/* Checkout Form */}
            <form 
              className={css`
                display: inline-block;
                text-align: center;
                maxWidth: 450px;
              `}
              onSubmit={this.handleConfirmOrder}
            >
              {/* Shipping Address Input */}
              <TextField
                id="address"
                name="address"
                type="text"
                placeholder="Shipping Address"
                onChange={this.handleChange}
                value={address}
              />
              {/* Postal Code Input */}
              <TextField
                id="postalCode"
                name="postalCode"
                type="text"
                placeholder="Postal Code"
                onChange={this.handleChange}
                value={postalCode}
              />
              {/* City Input */}
              <TextField
                id="city"
                name="city"
                type="text"
                placeholder="City of Residence"
                onChange={this.handleChange}
                value={city}
              />
              {/* Confirmation Email Address Input */}
              <TextField
                id="confirmationEmailAddress"
                name="confirmationEmailAddress"
                type="email"
                placeholder="Confirmation Email Address"
                onChange={this.handleChange}
                value={confirmationEmailAddress}
              />    
              {/* Credit Card Element */}
              <CardElement id="stripe__input" onReady={input => input.focus()}/>
              <button id="stripe__button" type="submit">Submit</button>
            </form>
          </React.Fragment> : (
            // Default Text if No Items in Cart
            <Box color="darkWash" shape="rounded" padding={4}>
              <Heading align="center" color="watermelon" size="xs">
                Your Cart is Empty
              </Heading>
              <Text align="center" italic color="green">
                Add some brews!
              </Text>
            </Box>
          )}
        </Box>
        {/* Confirmation Model */}
        {modal && (
          <ConfirmationModal 
            orderProcessing={orderProcessing}
            cartItems={cartItems}
            closeModal={this.closeModal}
            handleSubmitOrder={this.handleSubmitOrder} 
          />
        )}
        <ToastMessage show={toastShow} message={toastMessage} />
      </Container>      
    )
  }
}


const CheckoutForm = withRouter(injectStripe(_CheckoutForm));

const Checkout = () => (
  <StripeProvider apiKey="pk_test_0d9x3lfpcDSuVAcuMFceY0ao">
    <Elements>
      <CheckoutForm />
    </Elements>
  </StripeProvider>
);

export default Checkout;
