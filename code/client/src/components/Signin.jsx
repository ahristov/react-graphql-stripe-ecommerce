import React, { Component } from 'react';
import { Container, Box, Button, Heading, TextField } from 'gestalt';
import { css } from 'emotion';
import ToastMessage from './ToastMessage';
import { setToken } from '../utils';

import Strapi from 'strapi-sdk-javascript/build/main';
const apiUrl = process.env.API_URL || 'http://192.168.0.16:1337';
const strapi = new Strapi(apiUrl);

class Signin extends Component {
  state = {
    username: '',
    password: '',
    toastShow: false,
    toastMessage: '',
    loading: false
  };

  handleChange = ({ event, value }) => {
    event.persist();
    this.setState({ [event.target.name]: value });
  };

  handleSubmit = async event => {
    event.preventDefault();

    const { username, password } = this.state;

    if (this.isFormEmpty(this.state))
    {
      this.showToast('Fill in all fields');
      return;
    }

    // Sign up the user
    try
    {
      this.setState({ loading: true});
      const response = await strapi.login(username, password);
      this.setState({ loading: false});
      setToken(response.jwt);
      console.log(response);
      this.redirectUser('/');
    }
    catch(err) {
      this.setState({ loading: false});
      this.showToast(err.message);
    }
  }

  redirectUser = path => this.props.history.push(path);

  isFormEmpty = ({ username, password }) => {
    return !username || !password;
  }

  showToast = toastMessage => {
    this.setState({ toastShow: true, toastMessage });
    setInterval(() => this.setState({ toastShow: false, toastMessage: '' }), 5000);
  }

  render() {
    const { username, password, toastShow, toastMessage, loading } = this.state;

    return (
      <Container>
        <Box
          dangerouslySetInlineStyle={{
            __style: {
              backgroundColor: '#d6e3b1'
            }
          }}
          margin={4}
          padding={4}
          shape="rounded"
          display="flex"
          justifyContent="center"
        >
          {/* Sign In Form */}
          <form 
            className={css`
              display: inline-block;
              text-align: center;
              maxWidth: 450px;
            `}
            onSubmit={this.handleSubmit}
          >
            {/* Sign In Form Heading */}
            <Box
              marginBottom={2}
              display="flex"
              direction="column"
              alignItems="center"
            >
              <Heading color="midnight">Welcome back</Heading>
            </Box>
            {/* Username Input */}
            <TextField
              id="username"
              name="username"
              type="text"
              placeholder="Username"
              onChange={this.handleChange}
              value={username}
            />
            {/* Password Input */}
            <TextField
              id="password"
              name="password"
              type="password"
              placeholder="Password"
              onChange={this.handleChange}
              value={password}
            />
            <Button
              inline
              disabled={loading}
              color="blue"
              type="submit"
              text="Submit"
            />
          </form>
        </Box>
        <ToastMessage
          show={toastShow}
          message={toastMessage}
        />
      </Container>
    );
  }
}


export default Signin;
